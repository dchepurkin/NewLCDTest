#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "chrono"
#include "DCore.h"

// DCore Core;

void RenderThread()
{
    while (true)
    {
        // Core.RenderTick();
    }
}

int main()
{
    stdio_init_all();

    // multicore_launch_core1(RenderThread);
    // auto PreTime = std::chrono::high_resolution_clock::now();

    DScreen Screen;
    Screen.DrawRect({0, 0}, Screen.GetScreenSize(), 10, SNAKE_BORDER_COLOR, true, SNAKE_BACKGROUND_COLOR);
    

    // Screen.DrawCircle(Screen.GetScreenSize() / 2, 100, 10, RED);
    DVector2D Point1{0, 0};
    DVector2D Point2{0, 0};

    while (true)
    {
        // const auto CurrentTime = std::chrono::high_resolution_clock::now();
        // std::chrono::duration<float> DeltaTime = CurrentTime - PreTime;

        // Core.Tick(DeltaTime.count());

        // PreTime = CurrentTime;
        // sleep_ms(500);
        // Screen.FillScreen(RED);

        // sleep_ms(500);
        // Screen.DrawFillRect({0, 10}, {35, 35}, BLUE);

        // sleep_ms(500);
        // Screen.DrawFillRect({50, 45}, {30, 75}, YELLOW);

        sleep_ms(25);
        //Screen.DrawVLine(Point1, Screen.GetScreenSize().Y, RED);
        Screen.DrawHLine(Point2, Screen.GetScreenSize().X, BLUE);

        Point1.X += 2;
        Point2.Y += 2;
    }
}
