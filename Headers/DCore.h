#pragma once

#include "pico/stdlib.h"
#include "DTimerManager.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"
#include "SnakeGame/DSnakeGame.h"

class DCore
{
public:
    DCore();

    const DScreen &GetScreen() const { return Screen; }

    void RenderTick();

    void Tick(float DeltaTime);

    DTimerManager &GetTimerManager() { return TimerManager; }

private:
    DCore(const DCore &) = delete;

    DCore &operator=(const DCore &) = delete;

    DScreen Screen;

    DTimerManager TimerManager;

    DController Controller{this};

    DSnakeGame SnakeGame{&Controller, &Screen};
};