#pragma once

#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "Screen/DScreenTypes.h"
#include "Screen/DColors.h"

class DScreen
{
public:
    DScreen() : DScreen(DScreenInfo{}){};

    DScreen(const DScreenInfo &InScreenInfo);

    DScreen(const DScreen &) = delete;

    DScreen &operator=(const DScreen &) = delete;

    ~DScreen() = default;

    uint GetPixelsCount() const { return ScreenInfo.Size.X * ScreenInfo.Size.Y; }

    uint GetWidth() const { return ScreenInfo.Size.X; }

    uint GetHeight() const { return ScreenInfo.Size.Y; }

    const DVector2D &GetScreenSize() const { return ScreenInfo.Size; }

    void SetBackgroundColor(const uint16_t InColor);

    uint16_t GetBackgroundColor() const { return BackgroundColor; }

    void DrawPixel(const DVector2D &InPoint, const uint16_t InColor);

    void DrawLine(const DVector2D &InStartPoint, const DVector2D &InEndPoint, const uint16_t InColor);

    void DrawHLine(const DVector2D &InStartPoint, const uint InLength, const uint16_t InColor);

    void DrawVLine(const DVector2D &InStartPoint, const uint InLength, const uint16_t InColor);

    void DrawRect(const DVector2D &InPoint, const DVector2D &InSize, const uint InBorderSize, const uint16_t InBorderColor, const bool Fill = false, const uint16_t InFillColor = 0xFFFF);

    void DrawFillRect(const DVector2D &InPoint, const DVector2D &InSize, const uint16_t InColor);

    void FillScreen(const uint16_t InColor);

    void DrawCircle(const DVector2D &InCenter, const uint InRadius, const uint InBorderSize, const uint16_t InColor);

    void DrawFillCircle(const DVector2D &InCenter, const uint InRadius, const uint16_t InColor);

    void SetLedEnabled(const bool IsEnabled);

private:
    uint16_t BackgroundColor = WHITE;

    DScreenInfo ScreenInfo;

    PIO Pio = pio0;

    const uint SMNum = 0;

    void InitScreen();

    //Устанавливает область в которой будут обновлены пиксели
    void StartPixels(const DVector2D &InPoint1, const DVector2D &InPoint2);

    void SetDCCS(bool dc, bool cs);

    void WriteSeq(const uint8_t *InSeq);

    void WriteCMD(const uint8_t *InCmd, size_t InCount);

    uint GetBufferCoords(const DVector2D &InPoint);

    void PutPixel(const uint16_t InColor);
};