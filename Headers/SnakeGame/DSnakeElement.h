#pragma once

#include "Screen/DScreenTypes.h"

class DSnakeElement
{
public:
    DSnakeElement(class DScreen *InScreen, const int InGridSize);

    void SetLocation(const DVector2D &InNewLocation);

    void AddChild(DSnakeElement *InSnakeElement);

    void Render();

private:
    class DScreen *Screen = nullptr;

    DVector2D Location{-1, -1};

    DSnakeElement *Next = nullptr;

    int GridSize;
};