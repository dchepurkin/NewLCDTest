#pragma once

#include "pico/stdlib.h"
#include "DTimerHandle.h"
#include "SnakeGame/DSnakeHead.h"

class DSnakeGame
{
public:
    DSnakeGame() = delete;

    explicit DSnakeGame(class DController* InController, class DScreen* InScreen);

    class DController* GetController() const { return Controller; }

    class DScreen* GetScreen() const { return Screen; }

    int GetGridSize() const { return GridSize; }

    void StartGame();

private:
    class DController* Controller = nullptr;

    class DScreen* Screen = nullptr;

    DSnakeHead Head;

    DTimerHandle GameTickTimerHandle;

    int GridSize = 10;

    float FrameRate = 0.125f;

    void GameTick();
};