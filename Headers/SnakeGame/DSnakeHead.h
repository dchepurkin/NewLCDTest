#pragma once

#include "pico/stdlib.h"
#include "Screen/DScreenTypes.h"
#include "Delegates/DDelegate.h"
#include <map>

DECLARE_DELEGATE(OnDeathSignature);

enum class EDirection
{
    Up,
    Down,
    Right,
    Left
};

class DSnakeHead
{
public:
    OnDeathSignature OnDeath;

    DSnakeHead() = default;

    void Init(class DSnakeGame *Ingame);

    void Tick();

    void Render();

    void Move();

private:
    class DSnakeGame *Game = nullptr;

    class DController *Controller = nullptr;

    class DScreen *Screen = nullptr;

    class DSnakeElement *Next = nullptr;

    class DSnakeElement *Last = nullptr;

    int GridSize;

    uint SnakeSize = 8;

    DVector2D Location;

    EDirection CurrentDirection = EDirection::Up;

    EDirection LastDirection = EDirection::Up;

    std::map<EDirection, DVector2D> DirectionMap{
        {EDirection::Up, DVector2D{0, -1}},
        {EDirection::Down, DVector2D{0, 1}},
        {EDirection::Left, DVector2D{-1, 0}},
        {EDirection::Right, DVector2D{1, 0}}};

    void SpawnElement(const DVector2D &InLocation);

    void OnUpButtonCallback(bool IsPressed);

    void OnDownButtonCallback(bool IsPressed);

    void OnLeftButtonCallback(bool IsPressed);

    void OnRightButtonCallback(bool IsPressed);
};