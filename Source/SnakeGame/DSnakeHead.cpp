#include "SnakeGame/DSnakeHead.h"
#include "SnakeGame/DSnakeGame.h"
#include "Screen/DScreen.h"
#include "Controller/DController.h"
#include "Controller/DButton.h"
#include "SnakeGame/DSnakeElement.h"

void DSnakeHead::Init(DSnakeGame *InGame)
{
    Game = InGame;

    Controller = Game->GetController();
    Screen = Game->GetScreen();
    GridSize = Game->GetGridSize();

    Location = Screen->GetScreenSize() / GridSize / 2;

    for (uint i = 0; i < SnakeSize; ++i)
    {
        SpawnElement(Location);
    }

    Controller->UpButton.Bind(this, &DSnakeHead::OnUpButtonCallback);
    Controller->DownButton.Bind(this, &DSnakeHead::OnDownButtonCallback);
    Controller->LeftButton.Bind(this, &DSnakeHead::OnLeftButtonCallback);
    Controller->RightButton.Bind(this, &DSnakeHead::OnRightButtonCallback);
}

void DSnakeHead::Tick()
{
    Move();
    Render();
}

void DSnakeHead::Render()
{
    DVector2D Point = Location * GridSize;
    DVector2D Size{GridSize, GridSize};
    Screen->DrawRect(Point, Size, 2, SNAKE_HEAD_BORDER_COLOR, true, SNAKE_HEAD_COLOR);
}

void DSnakeHead::Move()
{
    if (Next)
    {
        Next->SetLocation(Location);
        Next->Render();
    }

    Location = Location + DirectionMap[CurrentDirection];
    LastDirection = CurrentDirection;
}

void DSnakeHead::SpawnElement(const DVector2D &InLocation)
{
    if (const auto NewElement = new DSnakeElement(Screen, GridSize))
    {
        if (Last)
        {
            Last->AddChild(NewElement);
        }

        Last = NewElement;

        if (!Next)
        {
            Next = NewElement;
        }
    }
}

void DSnakeHead::OnUpButtonCallback(bool IsPressed)
{
    if(IsPressed)
    {
        if(CurrentDirection != EDirection::Down && LastDirection != EDirection::Down)
        {
            CurrentDirection = EDirection::Up;
        }
    }
}

void DSnakeHead::OnDownButtonCallback(bool IsPressed)
{
    if(IsPressed)
    {
        if(CurrentDirection != EDirection::Up && LastDirection != EDirection::Up)
        {
            CurrentDirection = EDirection::Down;
        }
    }
}

void DSnakeHead::OnLeftButtonCallback(bool IsPressed)
{
    if(IsPressed)
    {
        if(CurrentDirection != EDirection::Right && LastDirection != EDirection::Right)
        {
            CurrentDirection = EDirection::Left;
        }
    }
}

void DSnakeHead::OnRightButtonCallback(bool IsPressed)
{
    if(IsPressed)
    {
        if(CurrentDirection != EDirection::Left && LastDirection != EDirection::Left)
        {
            CurrentDirection = EDirection::Right;
        }
    }
}
