#include "SnakeGame/DSnakeGame.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"

DSnakeGame::DSnakeGame(DController *InController, DScreen *InScreen)
{
    Controller = InController;
    Screen = InScreen;
}

void DSnakeGame::StartGame()
{
    Head.Init(this);

    DVector2D Point{0, 0};
    Screen->DrawRect({0, 0}, Screen->GetScreenSize(), 10, SNAKE_BORDER_COLOR, true, SNAKE_BACKGROUND_COLOR);

    Controller->GetTimerManager().SetTimer(GameTickTimerHandle, this, &DSnakeGame::GameTick, FrameRate, true);
}

void DSnakeGame::GameTick()
{
    Head.Tick();
    //Screen->SetScreenChanged();
}