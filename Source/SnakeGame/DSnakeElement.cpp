#include "SnakeGame/DSnakeElement.h"
#include "Screen/DScreen.h"

DSnakeElement::DSnakeElement(DScreen *InScreen, const int InGridSize)
{
    Screen = InScreen;
    GridSize = InGridSize;
}

void DSnakeElement::SetLocation(const DVector2D &InNewLocation)
{
    if (!Next)
    {
        DVector2D Point = Location * GridSize;
        DVector2D Size{GridSize, GridSize};
        Screen->DrawFillRect(Point, Size, Screen->GetBackgroundColor());
    }
    else
    {
        Next->SetLocation(Location);
    }

    Location = InNewLocation;
}

void DSnakeElement::AddChild(DSnakeElement *InSnakeElement)
{
    Next = InSnakeElement;
    Next->SetLocation(Location);
}

void DSnakeElement::Render()
{
    DVector2D Point = Location * GridSize;
    DVector2D Size{GridSize, GridSize};
    Screen->DrawRect(Point, Size, 2, SNAKE_ELEMENT_BORDER_COLOR, true, SNAKE_ELEMENT_COLOR);
}
