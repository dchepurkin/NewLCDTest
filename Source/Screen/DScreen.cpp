#include "Screen/DScreen.h"
#include <cmath>
#include "st7789_lcd.pio.h"

DScreen::DScreen(const DScreenInfo &InScreenInfo)
{
    ScreenInfo = InScreenInfo;

    uint Offset = pio_add_program(Pio, &st7789_lcd_program);
    st7789_lcd_program_init(Pio, SMNum, Offset, ScreenInfo.PinDIN, ScreenInfo.PinCLK, ScreenInfo.SerialClkDiv);

    gpio_init(ScreenInfo.PinCS);
    gpio_init(ScreenInfo.PinDC);
    gpio_init(ScreenInfo.PinRESET);
    gpio_init(ScreenInfo.PinLED);
    gpio_set_dir(ScreenInfo.PinCS, GPIO_OUT);
    gpio_set_dir(ScreenInfo.PinDC, GPIO_OUT);
    gpio_set_dir(ScreenInfo.PinRESET, GPIO_OUT);
    gpio_set_dir(ScreenInfo.PinLED, GPIO_OUT);

    gpio_put(ScreenInfo.PinCS, true);
    gpio_put(ScreenInfo.PinRESET, true);
    gpio_put(ScreenInfo.PinLED, false);
    InitScreen();

    FillScreen(BackgroundColor);
    SetLedEnabled(true);
}

void DScreen::SetBackgroundColor(const uint16_t InColor)
{
    BackgroundColor = InColor;
}

void DScreen::DrawPixel(const DVector2D &InPoint, const uint16_t InColor)
{
    if (InPoint.X >= GetWidth() || InPoint.Y >= GetHeight() || InPoint.X < 0 || InPoint.Y < 0)
        return;

    StartPixels(InPoint, InPoint);
    PutPixel(InColor);
    SetDCCS(true, true);
}

void DScreen::DrawLine(const DVector2D &InStartPoint, const DVector2D &InEndPoint, const uint16_t InColor)
{
    if (InEndPoint.X < InStartPoint.X)
    {
        DrawLine(InEndPoint, InStartPoint, InColor);
        return;
    }

    if (InStartPoint == InEndPoint)
    {
        DrawPixel(InStartPoint, InColor);
        return;
    }

    if (InStartPoint.IsXEqual(InEndPoint))
    {
        DrawVLine(InStartPoint, InEndPoint.Y - InStartPoint.Y, InColor);
        return;
    }

    if (InStartPoint.IsYEqual(InEndPoint))
    {
        DrawHLine(InStartPoint, InEndPoint.X - InStartPoint.X, InColor);
        return;
    }

    const int DX = InEndPoint.X - InStartPoint.X;
    int DY = abs(InEndPoint.Y - InStartPoint.Y) + 1;
    int D = 0;
    int DirY = (int)(InEndPoint.Y - InStartPoint.Y) > 0 ? 1 : -1;

    for (DVector2D Point(InStartPoint); Point.X <= InEndPoint.X; ++Point.X)
    {
        DrawPixel(Point, InColor);
        D += DY;
        if (D >= DX)
        {
            Point.Y += DirY;
            D -= DX;
        }
    }
}

void DScreen::DrawHLine(const DVector2D &InStartPoint, const uint InLength, const uint16_t InColor)
{
    auto FinishX = InStartPoint.X + InLength;
    const auto Width = GetWidth();
    if (FinishX > Width)
    {
        FinishX = Width;
    }

    StartPixels(InStartPoint, {FinishX, InStartPoint.Y}); 

    for (uint X = InStartPoint.X; X < FinishX; ++X)
    {
        PutPixel(InColor);
    }

     SetDCCS(true, true);
}

void DScreen::DrawVLine(const DVector2D &InStartPoint, const uint InLength, const uint16_t InColor)
{
    auto FinishY = InStartPoint.Y + InLength;
    const auto Height = GetHeight();
    if (FinishY > Height)
    {
        FinishY = Height;
    }

    StartPixels(InStartPoint, {InStartPoint.X, FinishY});    

    for (uint Y = InStartPoint.Y; Y < FinishY; ++Y)
    {
        PutPixel(InColor);
    }

    SetDCCS(true, true);
}

void DScreen::DrawRect(const DVector2D &InPoint, const DVector2D &InSize, const uint InBorderSize, const uint16_t InBorderColor, const bool Fill, const uint16_t InFillColor)
{
    if (InBorderColor == InFillColor || InBorderSize * 2 >= InSize.X || InBorderSize * 2 >= InSize.Y)
    {
        DrawFillRect(InPoint, InSize, InBorderColor);
        return;
    }

    if (InBorderSize == 0)
    {
        if (Fill)
        {
            DrawFillRect(InPoint, InSize, InFillColor);
        }

        return;
    }

    // Top Border
    DVector2D Point(InPoint);
    DVector2D Size(InSize.X, InBorderSize);
    DrawFillRect(Point, Size, InBorderColor);

    // Left Border
    Point.Set(InPoint.X, InPoint.Y + InBorderSize);
    Size.Set(InBorderSize, InSize.Y - InBorderSize * 2);
    DrawFillRect(Point, Size, InBorderColor);

    // Right Border
    Point.Set(InPoint.X + InSize.X - InBorderSize, InPoint.Y + InBorderSize);
    Size.Set(InBorderSize, InSize.Y - InBorderSize * 2);
    DrawFillRect(Point, Size, InBorderColor);

    // Bottom Border
    Point.Set(InPoint.X, InPoint.Y + InSize.Y - InBorderSize);
    Size.Set(InSize.X, InBorderSize);
    DrawFillRect(Point, Size, InBorderColor);

    // Fill
    if (Fill)
    {
        Point.Set(InPoint.X + InBorderSize, InPoint.Y + InBorderSize);
        Size.Set(InSize.X - 2 * InBorderSize, InSize.Y - 2 * InBorderSize);
        DrawFillRect(Point, Size, InFillColor);
    }
}

void DScreen::DrawFillRect(const DVector2D &InPoint, const DVector2D &InSize, const uint16_t InColor)
{
    const auto Height = GetHeight();
    const auto Width = GetWidth();

    if (InPoint.X >= Width || InPoint.Y >= Height)
        return;

    auto FinishPoint = InPoint + InSize;

    StartPixels(InPoint, FinishPoint);

    const auto PixeldCount = InSize.X * InSize.Y;

    for (uint i = 0; i < PixeldCount; ++i)
    {
        PutPixel(InColor);
    }

    SetDCCS(true, true);
}

void DScreen::FillScreen(const uint16_t InColor)
{
    DrawFillRect({0, 0}, GetScreenSize(), InColor);
}

void DScreen::DrawCircle(const DVector2D &InCenter, const uint InRadius, const uint InBorderSize, const uint16_t InColor)
{
    if (InBorderSize == 0)
    {
        return;
    }

    for (uint i = 0; i < InBorderSize; ++i)
    {
        int X = 0;
        int Y = InRadius - i;
        int Delta = 1 - 2 * Y;
        int Error = 0;

        while (Y >= X)
        {
            DVector2D Point1(InCenter.X + X, InCenter.Y + Y);
            DVector2D Point2(InCenter.X - X, InCenter.Y + Y);
            DVector2D Point3(InCenter.X + X, InCenter.Y - Y);
            DVector2D Point4(InCenter.X - X, InCenter.Y - Y);
            DVector2D Point5(InCenter.X + Y, InCenter.Y + X);
            DVector2D Point6(InCenter.X - Y, InCenter.Y + X);
            DVector2D Point7(InCenter.X + Y, InCenter.Y - X);
            DVector2D Point8(InCenter.X - Y, InCenter.Y - X);

            DrawPixel(Point1, InColor);
            DrawPixel(Point2, InColor);
            DrawPixel(Point3, InColor);
            DrawPixel(Point4, InColor);
            DrawPixel(Point5, InColor);
            DrawPixel(Point6, InColor);
            DrawPixel(Point7, InColor);
            DrawPixel(Point8, InColor);

            Error = 2 * (Delta + Y) - 1;
            if (Delta < 0 && Error <= 0)
            {
                Delta += 2 * ++X + 1;
                continue;
            }

            if (Delta > 0 && Error > 0)
            {
                Delta -= 2 * --Y + 1;
                continue;
            }

            Delta += 2 * (++X - --Y);
        }
    }
}

void DScreen::DrawFillCircle(const DVector2D &InCenter, const uint InRadius, const uint16_t InColor)
{
    int X = 0;
    int Y = InRadius;
    int Delta = 1 - 2 * Y;
    int Error = 0;

    while (Y >= X)
    {
        DVector2D Point1(InCenter.X + X, InCenter.Y + Y);
        DVector2D Point2(InCenter.X - X, InCenter.Y + Y);
        DVector2D Point3(InCenter.X + X, InCenter.Y - Y);
        DVector2D Point4(InCenter.X - X, InCenter.Y - Y);
        DVector2D Point5(InCenter.X + Y, InCenter.Y + X);
        DVector2D Point6(InCenter.X - Y, InCenter.Y + X);
        DVector2D Point7(InCenter.X + Y, InCenter.Y - X);
        DVector2D Point8(InCenter.X - Y, InCenter.Y - X);

        DrawHLine(Point2, Point1.X - Point2.X, InColor);
        DrawHLine(Point4, Point3.X - Point4.X, InColor);
        DrawHLine(Point6, Point5.X - Point6.X, InColor);
        DrawHLine(Point8, Point7.X - Point8.X, InColor);

        Error = 2 * (Delta + Y) - 1;
        if (Delta < 0 && Error <= 0)
        {
            Delta += 2 * ++X + 1;
            continue;
        }

        if (Delta > 0 && Error > 0)
        {
            Delta -= 2 * --Y + 1;
            continue;
        }

        Delta += 2 * (++X - --Y);
    }
}

void DScreen::SetLedEnabled(const bool IsEnabled)
{
    gpio_put(ScreenInfo.PinLED, IsEnabled);
}

void DScreen::InitScreen()
{
    const uint8_t InitSeq[] = {
        5, 0, 0xF7, 0xA9, 0x51, 0x2C, 0x82,
        3, 0, 0xC0, 0x11, 0x09,
        2, 0, 0xC1, 0x41,
        4, 0, 0xC5, 0x00, 0x0A, 0x80,
        3, 0, 0xB1, 0xB0, 0x11,
        2, 0, 0xB4, 0x02,
        3, 0, 0xB6, 0x02, 0x22,
        2, 0, 0xB7, 0xC6,
        3, 0, 0xBE, 0x00, 0x04,
        2, 0, 0xE9, 0x00,
        2, 0, 0x36, 0x08,
        2, 0, 0x3A, 0x66,
        16, 0, 0xE0, 0x00, 0x07, 0x10, 0x09, 0x17, 0x0B, 0x41, 0x89, 0x4B, 0x0A, 0x0C, 0x0E, 0x18, 0x1B, 0x0F,
        16, 0, 0xE1, 0x00, 0x17, 0x1A, 0x04, 0x0E, 0x06, 0x2F, 0x45, 0x43, 0x02, 0x0A, 0x09, 0x32, 0x36, 0x0F,
        1, 10, 0x11,
        1, 0, 0x29,
        0 //
    };

    WriteSeq(InitSeq);
}

void DScreen::StartPixels(const DVector2D &InPoint1, const DVector2D &InPoint2)
{
    DVector2D FinishPoint;
    FinishPoint.X = InPoint2.X > InPoint1.X ? InPoint2.X - 1 : InPoint2.X;
    FinishPoint.Y = InPoint2.Y > InPoint1.Y ? InPoint2.Y - 1 : InPoint2.Y;

    const uint8_t Seq[] = {
        5, 0, 0x2a, uint8_t(InPoint1.X >> 8), uint8_t(InPoint1.X), (uint8_t)((FinishPoint.X) >> 8), (uint8_t)(FinishPoint.X),
        5, 0, 0x2b, uint8_t(InPoint1.Y >> 8), uint8_t(InPoint1.Y), (uint8_t)((FinishPoint.Y) >> 8), (uint8_t)(FinishPoint.Y),
        1, 0, 0x2C, //
        0           //
    };

    WriteSeq(Seq);
    SetDCCS(true, false);
}

void DScreen::SetDCCS(bool DC, bool CS)
{
    sleep_us(1);
    gpio_put_masked((1u << ScreenInfo.PinDC) | (1u << ScreenInfo.PinCS), !!DC << ScreenInfo.PinDC | !!CS << ScreenInfo.PinCS);
    sleep_us(1);
}

void DScreen::WriteSeq(const uint8_t *InSeq)
{
    while (*InSeq)
    {
        WriteCMD(InSeq + 2, *InSeq);
        sleep_ms(*(InSeq + 1) * 5);
        InSeq += *InSeq + 2;
    }
}

void DScreen::WriteCMD(const uint8_t *InCmd, size_t InCount)
{
    st7789_lcd_wait_idle(Pio, SMNum);
    SetDCCS(false, false);
    st7789_lcd_put(Pio, SMNum, *InCmd++);

    if (InCount >= 2)
    {
        st7789_lcd_wait_idle(Pio, SMNum);
        SetDCCS(true, false);
        for (uint i = 0; i < InCount - 1; ++i)
            st7789_lcd_put(Pio, SMNum, *InCmd++);
    }

    st7789_lcd_wait_idle(Pio, SMNum);
    SetDCCS(true, true);
}

uint DScreen::GetBufferCoords(const DVector2D &InPoint)
{
    return InPoint.Y * GetWidth() + InPoint.X;
}

void DScreen::PutPixel(const uint16_t InColor)
{
    st7789_lcd_put(Pio, SMNum, (InColor >> 8) & 0xF8);
    st7789_lcd_put(Pio, SMNum, (InColor >> 3) & 0xFC);
    st7789_lcd_put(Pio, SMNum, uint8_t(InColor << 3));
}
