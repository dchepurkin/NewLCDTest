#include "DTimerManager.h"
#include "DCore.h"

void DTimerManager::Tick(float DeltaTime)
{
    for (auto Handle : HandleList)
    {
        if (!Handle->IsActive())
        {
            continue;
        }

        Handle->Tick(DeltaTime);
    }
}

void DTimerManager::ClearTimer(DTimerHandle &InTimerHandle)
{
    auto It = std::remove(HandleList.begin(), HandleList.end(), &InTimerHandle);
    HandleList.erase(It, HandleList.end());

    InTimerHandle.Clear();
}
