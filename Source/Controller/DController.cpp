#include "Controller/DController.h"
#include "DCore.h"

DController::DController(class DCore *InGameCore)
{
    Core = InGameCore;
}

DTimerManager &DController::GetTimerManager()
{
    return Core->GetTimerManager();
}

void DController::Tick(float DeltaTime)
{
}