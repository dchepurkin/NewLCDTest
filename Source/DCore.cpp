#include "DCore.h"

DCore::DCore()
{
    SnakeGame.StartGame();
}

void DCore::RenderTick()
{
    
}

void DCore::Tick(float DeltaTime)
{
    TimerManager.Tick(DeltaTime);
    Controller.Tick(DeltaTime);
}
